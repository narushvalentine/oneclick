$(function(){

    $("#responsive-box").dxResponsiveBox({
        rows: [
            { ratio: 0 },
            { ratio: 2 },
            { ratio: 2, screen: "xs" },
            { ratio: 0 }
        ],
        cols: [
            { ratio: 0 },
            { ratio: 4, screen: "lg" },
            { ratio: 1 }
        ],
        singleColumnScreen: "sm",
        screenByWidth: function(width) {
            return ( width < 700 ) ? 'sm' : 'lg';
        }
    });

/*
    $("#boxOptions3").dxBox({
        direction: "col",
        width: "100%",
        height: "100%",

    });

    $("#boxOptions4").dxBox({
        direction: "row",
        width: "100%",
        height: "100%"
    });
    $("#boxOptions5").dxBox({
        direction: "row",
        width: "100%",
        height: 900
    });
*/
    $("#success").dxButton({
        text: "+375295416584",
        type: "success",
        icon: 'tel',
        onClick: function(e) {
            DevExpress.ui.notify("The Apply button was clicked");
        }
    });

    $("#default").dxButton({
        text: "CHAT",
        type: "default",
        icon: 'group',
        onClick: function(e) {
            DevExpress.ui.notify("The Done button was clicked");
        }
    });

    $("#favorites").dxButton({
        text: "CHAT",
        icon: 'favorites',
        onClick: function(e) {
            DevExpress.ui.notify("The Back button was clicked");
        }
    });


    var radioGroupItems = [
        { text: "Item 1", color: "grey" }
    ];
    $(function () {
        $("#radioGroup").dxRadioGroup({
            dataSource: radioGroupItems,
            displayExpr: "text",
            valueExpr: "color",
            value: "green"
        });
    });


    var options = {
        max_value: 3, // кол-во звезд
        step_size: 1, //шаг
        initial_value: 3 // кол0во звезд выделенных
    };
    $("#star6").rate(options);


    var options1 = {
        max_value: 3, // кол-во звезд
        step_size: 1, //шаг
        initial_value: 3 // кол0во звезд выделенных
    };
    $(".rate3").rate(options1);

    var options2 = {
        max_value: 4, // кол-во звезд
        step_size: 1, //шаг
        initial_value: 4 // кол0во звезд выделенных
    };
    $(".rate4").rate(options2);

    var options3 = {
        max_value: 5, // кол-во звезд
        step_size: 1, //шаг
        initial_value: 5 // кол0во звезд выделенных
    };
    $(".rate5").rate(options3);


// MAPS
    var map = $("#map").dxMap({
        zoom: 14,
        height: "100%",
        width: "100%",
        controls: true,
        markers: [{
            location: "40.7825, -73.966111"
        }, {
            location: [40.755833, -73.986389]
        }, {
            location: { lat: 40.753889, lng: -73.981389}
        }, {
            location: "Brooklyn Bridge,New York,NY"
        }
        ],
        routes: [
            {
                weight: 6,
                color: "blue",
                opacity: 0.5,
                mode: "",
                locations: [
                    [40.782500, -73.966111],
                    [40.755833, -73.986389],
                    [40.753889, -73.981389],
                    "Brooklyn Bridge,New York,NY"
                ]

            }
        ]
    }).dxMap("instance");

    $("#choose-mode").dxSelectBox({
        dataSource: [ "driving", "walking" ],
        value: "driving",
        onValueChanged: function(data) {
            map.option("routes", [ $.extend({}, map.option("routes")[0], {
                mode: data.value
            }) ]);
        }
    });

    $("#choose-color").dxSelectBox({
        dataSource: [ "blue", "green", "red" ],
        value: "blue",
        onValueChanged: function (data) {
            map.option("routes", [ $.extend({}, map.option("routes")[0], {
                color: data.value
            }) ]);
        }
    });

});